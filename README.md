Here you can find the simulations used in the publication:

## [In search of the Goldilocks zone for hybrid speciation](https://doi.org/10.1371/journal.pgen.1007613)

Check out the paper published in _Plos Genetics_: [In search of the Goldilocks zone for hybrid speciation](https://doi.org/10.1371/journal.pgen.1007613) or the [preprint version](https://doi.org/10.1101/266254) of the paper.

The simulations have been written using C++ and require the GSL - GNU Scientific Library.

Learn more about the [GNU Scientific Library](https://www.gnu.org/software/gsl/)

Binaries for both Linux (x86_64) and mac OS are provided in the sub-folders of `4loci/bin/`.
A Dockerfile is also provided in the `4loci/` folder so you can run the Linux version on any system that runs Docker.

### Compilation of the source code (if you can't run the binary version)

#### Make sure the GNU Scientific Library is installed
For macOS you could use [homebrew](www.brew.sh) and install GSL via:
`brew install gsl`

Precompiled binary packages are included in most GNU/Linux distributions.
For further information visit the [GNU Scientific Library web-site](https://www.gnu.org/software/gsl/) 

Go to the `4loci` folder

To create an executable compile the source code, on macOS run:
`g++ -Wall -lgsl -O3  main2.cpp -o fourloci`

To statically link GSL we used:
`g++ -mmacosx-version-min=10.9 -v main2.cpp -std=gnu++11 /usr/local/opt/gsl/lib/libgsl.a /usr/local/opt/gsl/lib/libgslcblas.a`

On Linux build binary using the following command:
`g++ -o fourloci main2.cpp -std=gnu++11 $(gsl-config --cflags --libs)`

For static linking of the libraries:
`g++ -o fourloci-static main2.cpp -static -std=gnu++11 $(gsl-config --cflags --libs)`

Check that the file `run_fourloci.sh` is executable.

To make it executable use:
`chmod +x run_fourloci.sh`

To run a simulation:
`./run_fourloci.sh`

To analyse the dataset, use the following R notebook:
`analysis.Rmd`

See this [web-site](https://evoldyn.gitlab.io/four-loci/) for an example output of the RStudio notebook

The code to reproduce the various figures from the mansucript is given in the following R notebook:
`Final_version.Rmd`

---

In the directory `evalutation_of_the_barrier_strength` you can find the extendend version of the simulation. The binaries can also be found in the folder `4loci/bin/`.

For the genetic barrier version the binaries were built using:

```bash
# on macOS inside evalutation_of_the_barrier_strength/ directory run
# you may need to first run `brew install gsl`
g++ -mmacosx-version-min=10.9 -v -Wall -lgsl -O3 genetic_barrier.cpp -o barrier 


# on Linux build binary using the following command:
g++ -o barrier genetic_barrier.cpp -std=gnu++11 $(gsl-config --cflags --libs)

# on Linux for static linking of the libraries run:
g++ -o barrier-static genetic_barrier.cpp -static -std=gnu++11 $(gsl-config --cflags --libs)
```