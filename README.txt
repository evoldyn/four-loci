The simulations have been written using C++ and require the GSL - GNU Scientific Library.

https://www.gnu.org/software/gsl/

For macOS you could use homebrew from www.brew.sh and install GSL via:
brew install gsl

Go to the 4loci folder

To create an executable compile the source code, on macOS run:
g++ -Wall -lgsl -O3  main2.cpp -o fourloci

To statically link GSL we used:
g++ -mmacosx-version-min=10.9 -v main2.cpp -std=gnu++11 /usr/local/opt/gsl/lib/libgsl.a /usr/local/opt/gsl/lib/libgslcblas.a

On Linux build binary using the following command:
g++ -o fourloci main2.cpp -std=gnu++11 `gsl-config --cflags --libs`

For static linking of the libraries:
g++ -o fourloci-static main2.cpp -static -std=gnu++11 `gsl-config --cflags --libs`

Check that the file run_fourloci.sh is executable.

To make it executable use:
chmod +x run_fourloci.sh

To run a simulation:
./run_fourloci.sh

To analyse the dataset, use the following R notebook:
analysis.Rmd 

See https://evoldyn.gitlab.io/four-loci/ for example output of the RStudio notebook

---

For the genetic barrier version the binaries were built using:
# on macOS inside evalutation_of_the_barrier_strength/ directory run
# you may need to first run `brew install gsl`
g++ -mmacosx-version-min=10.9 -v -Wall -lgsl -O3 genetic_barrier.cpp -o barrier 

# on Linux build binary using the following command:
g++ -o barrier genetic_barrier.cpp -std=gnu++11 `gsl-config --cflags --libs`

# on Linux for static linking of the libraries run:
g++ -o barrier-static genetic_barrier.cpp -static -std=gnu++11 `gsl-config --cflags --libs`
