# Build this Docker image with e.g. 
# docker build -t dmi_sim .

# In rare cases, we need to run `docker build --rm --no-cache .` to update those dependencies
# docker build --no-cache -t dmi_sim .

# To run the simulation with your own input use:
# You need four mandatory parameters: the parameter file name, the output file, number of iterations, and the seed
# Output will be written to the current directory 
# docker run --rm -v "$(pwd)":/dmi_io dmi_sim fourloci parameter.txt output.txt 1000 -1

# For the full codominance demo run:

# docker run --rm -v "$(pwd)"/output_sym_dom_rfunction_pi_0d5:/dmi_io/output_sym_dom_rfunction_pi_0d5 dmi_sim run_fourloci.sh

# Or enter the container with a terminal attached to troubleshoot 
# and your current working directory mapped to /dmi_io: ()

# docker run --rm -it -v "$(pwd)":/dmi_io dmi_sim /bin/bash

# The `barrier` version  
# needs four mandatory parameters:
# the parameter file name, the output file, the number of iterations, and the seed
# the parameter file contains the different parameters in this order 
# s1, s2, s3, s4, e12, e13, e14, e23, e24, e34, r12,r23,r34, h12, h13, h14, h23, h24, h34 
# optional parameters are the name of the trajectory file as well as a fitness file to save the fitness matrix
# see also evalutation_of_the_barrier_strength/README and the source code genetic_barrier.cpp

FROM alpine
RUN apk add --no-cache bash && mkdir /dmi_io
WORKDIR /dmi_io
COPY bin/linux_x86_64/*-static /usr/local/bin/
COPY run_fourloci*.sh /usr/local/bin/
RUN chmod -Rv u+x /usr/local/bin/

# inside the container in /usr/local/bin/ you can find the executables: 
# fourloci-static, barrier-static, run_fourloci.sh

# enter the container with an interactive terminal and 
# your current working directory mapped to /dmi_io using:
# docker run --rm -it -v "$(pwd)":/dmi_io dmi_sim /bin/bash