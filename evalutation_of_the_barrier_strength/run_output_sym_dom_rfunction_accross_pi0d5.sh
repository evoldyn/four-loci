#!/bin/bash

S1=0.001
S2=0.001
S3=0.001
S4=0.001

E13=-0.2
E12=0
E14=0
E23=0
E34=0
E24=-0.2

R12=0.5
R23=0.5
R34=0.5
R45=0.5

NGEN=10000000
NIND=10000

H12=1
H13=1
H14=1
H23=1
H24=1
H34=1

f1=0 #hap0000
f2=0 #hap0001
f3=0 #hap0010
f4=0 #hap0011
f5=0 #hap0100
f6=0 #hap0101 ie hap1
f7=0 #hap0110
f8=0 #hap0111
f9=0 #hap1000
f10=0.9998 #hap1001
f11=0 #hap1010 ie hap2
f12=0 #hap1011
f13=0 #hap1100
f14=0 #hap1101
f15=0 #hap1110
f16=0 #hap1111
#and with the neutral marker
f01=0 #hap0000
f02=0 #hap0001
f03=0 #hap0010
f04=0.0002 #hap0011
f05=0 #hap0100
f06=0 #hap0101 ie hap1
f07=0 #hap0110
f08=0 #hap0111
f09=0 #hap1000
f010=0 #hap1001
f011=0 #hap1010 ie hap2
f012=0 #hap1011
f013=0 #hap1100
f014=0 #hap1101
f015=0 #hap1110
f016=0 #hap1111


fmax=0.7 # maximum frequency for pop2
delta=0.005
nb_ite=1000000 # nb_ite per case

# define imin imax itop and inc as follows imax
itop=1000 # 1/order of magnitude of delta
imin=400 # 0.5 * itop
imax=600 # fmax*itop
inc=2 # delta*itop

name="output_sym_dom_rfunction_across_pi_0d5"
mkdir $name
cd $name

for (( i=1; i<=9; i+=1 ))
do
  test -f parameter.txt &&	rm -f parameter.txt
  echo "$S1 $S2 $S3 $S4 $E12 $E13 $E14 $E23 $E24 $E34 .00$i .00$i .00$i $R45 $H12 $H13 $H14 $H23 $H24 $H34 $NGEN $NIND $f1 $f2 $f3 $f4 $f5 $f6 $f7 $f8 $f9 $f10 $f11 $f12 $f13 $f14 $f15 $f16 $f01 $f02 $f03 $f04 $f05 $f06 $f07 $f08 $f09 $f010 $f011 $f012 $f013 $f014 $f015 $f016" >>parameter.txt
  echo ".$i"
  ./../barrier parameter.txt "${name}_rdot00${i}.txt" $nb_ite -1
done

for (( i=1; i<=9; i+=1 ))
do
  test -f parameter.txt &&	rm -f parameter.txt
  echo "$S1 $S2 $S3 $S4 $E12 $E13 $E14 $E23 $E24 $E34 .0$i .0$i .0$i $R45 $H12 $H13 $H14 $H23 $H24 $H34 $NGEN $NIND $f1 $f2 $f3 $f4 $f5 $f6 $f7 $f8 $f9 $f10 $f11 $f12 $f13 $f14 $f15 $f16 $f01 $f02 $f03 $f04 $f05 $f06 $f07 $f08 $f09 $f010 $f011 $f012 $f013 $f014 $f015 $f016" >>parameter.txt
  echo ".$i"
  ./../barrier parameter.txt "${name}_rdot0${i}.txt" $nb_ite -1
done

for (( i=1; i<=9; i+=1 ))
do
  test -f parameter.txt &&        rm -f parameter.txt
  echo "$S1 $S2 $S3 $S4 $E12 $E13 $E14 $E23 $E24 $E34 .000$i .000$i .000$i $R45 $H12 $H13 $H14 $H23 $H24 $H34 $NGEN $NIND $f1 $f2 $f3 $f4 $f5 $f6 $f7 $f8 $f9 $f10 $f11 $f12 $f13 $f14 $f15 $f16 $f01 $f02 $f03 $f04 $f05 $f06 $f07 $f08 $f09 $f010 $f011 $f012 $f013 $f014 $f015 $f016" >>parameter.txt
  echo ".$i"
  ./../barrier parameter.txt "${name}_rdot000${i}.txt" $nb_ite -1
done

for (( i=10; i<=50; i+=1 ))
do
 test -f parameter.txt &&        rm -f parameter.txt
 echo "$S1 $S2 $S3 $S4 $E12 $E13 $E14 $E23 $E24 $E34 .$i .$i .$i $R45 $H12 $H13 $H14 $H23 $H24 $H34 $NGEN $NIND $f1 $f2 $f3 $f4 $f5 $f6 $f7 $f8 $f9 $f10 $f11 $f12 $f13 $f14 $f15 $f16 $f01 $f02 $f03 $f04 $f05 $f06 $f07 $f08 $f09 $f010 $f011 $f012 $f013 $f014 $f015 $f016" >>parameter.txt
 echo ".$i"
 ./../barrier parameter.txt "${name}_rdot${i}.txt" $nb_ite -1
done