#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <array>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_randist.h>
#include <time.h>


using namespace std;

// to compile g++ -Wall -lgsl -O3  genetic_barrier.cpp -o barrier

int read_parameter(const char* input_file, double& s1, double& s2, double& s3, double& s4, double& e12, double& e13, double& e14, double& e23, double& e24, double& e34, double& r12, double& r23, double& r34, double& r45, int& h12, int& h13, int& h14, int& h23, int& h24, int& h34, int& n_gen, int& n_Ind, array<double, 32>& hap){

  int test;
  FILE * param_file;

    // read the parameter file; check that the different entries of the parameter file matches the expected type and store this value in the proper variable.

  param_file= fopen(input_file,"r");
  if (param_file == NULL) {
    cout << "wrong param file" << endl;
    return -1;
  }

  test=fscanf(param_file,"%lf ",&s1);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&s2);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&s3);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&s4);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&e12);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&e13);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&e14);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&e23);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&e24);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&e34);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&r12);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&r23);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&r34);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%lf ",&r45);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&h12);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&h13);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&h14);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&h23);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&h24);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&h34);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&n_gen);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  test=fscanf(param_file,"%i ",&n_Ind);
  if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
  for (int i=0; i<32; i++){
   test=fscanf(param_file, "%lf ", &hap[i]);
   if (test!=1){
    cout << "error param file"<<endl;
    return -1;
  }
}
fclose(param_file);
return 1;
}

void gametes_prod(array<double, 32>& hap, array<double, 1024>& gen, const double& rho12, const double& rho23, const double& rho34, const double& rho45){
    // define the gamete production by considering the allele at each locus from the paternal (i.m) and maternal (i.f) origin
  int indice, indicef, indicem, i1, i2,i3, i4, i5;
  for (int i1m=0; i1m<=1; i1m++){
    for (int i2m=0; i2m<=1; i2m++){
      for (int i3m=0; i3m<=1; i3m++ ){
        for (int i4m=0; i4m<=1; i4m++){
          for (int i1f=0; i1f<=1; i1f++){
            for (int i2f=0; i2f<=1; i2f++){
              for (int i3f=0; i3f<=1; i3f++ ){
                for (int i4f=0; i4f<=1; i4f++){
                  for (int i5m=0; i5m<=1; i5m++){
                    for (int i5f=0; i5f<=1; i5f++){
                  indicem =16*i5m+8*i1m+4*i2m+2*i3m+i4m; // define the paternal haplotype
                  indicef =16*i5f+8*i1f+4*i2f+2*i3f+i4f; // define the maternal haplotype
                  indice  =32*indicem+indicef;   // define the genotype
                    // define the genotype
                  i1=i1m+i1f;i2=i2m+i2f;i3=i3m+i3f;i4=i4m+i4f; i5=i5m+i5f; // define the status at each locus: 0 for 0/0, 1 for 0/1 or 1/0 and 2 for 1/1
                    // check that there is individuals of this genotype
                  if (gen[indice]>0){
                    // recombination matters only if there is at least two loci heterozygotes
                    // first assume that first locus is heterozygote
                    // cout << gen[indice]<< endl;
                    if(i1==1&&(i2==1||i3==1||i4==1||i5==1)){
                      // assume that the third and fourth loci are homozygote. Recombination only matter between loci 1 and 2
                      if (i4!=1&&i3!=1&&i5!=1){
                        hap[indicem]=hap[indicem]+ (1-rho12)*gen[indice]/2; // add half the non recombining frequency of the genotype to the haplotype matching the paternal one
                        hap[indicef]=hap[indicef]+ (1-rho12)*gen[indice]/2; // add half the non recombining frequency of the genotype to the haplotype matching the maternal one
                         // calculate the indices of the two recombinant haplotypes; recombination only matter if it happens between the first and second loci
                        indicem =8*i1m+4*i2f+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*gen[indice]/2; // add half of the frequency of the recombining genotype to the "first" haplotype
                        hap[indicef]=hap[indicef]+ rho12*gen[indice]/2; // add half of the frequency of the recombining genotype to the "second" haplotype
                        // cout << indice  << " rec 12" << endl;
                    // assume that the fourht locus is homozygote
                      } else if(i4!=1&&i5!=1){
                    // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho12)*(1-rho23)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho12)*(1-rho23)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 only
                        indicem =8*i1m+4*i2f+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*gen[indice]/2;
                         // recombination happens between loci 2 and 3 only
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 2 and 3
                        indicem =8*i1m+4*i2f+2*i3m+i4m+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3f+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*rho12*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho12*gen[indice]/2;
                        // cout << indice  << " rec 12 and 23" << endl;
                    // assume that the second, third  and fourth loci can be heterozygote
                      } else if (i5!=1){
                    // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho12)*(1-rho23)*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho12)*(1-rho23)*(1-rho34)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 only
                        indicem =8*i1m+4*i2f+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*(1-rho34)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 only
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*(1-rho34)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 2 and 3
                        indicem =8*i1m+4*i2f+2*i3m+i4m+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3f+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*rho12*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho12*(1-rho34)*gen[indice]/2;
                                            // recombination happens between loci 3 and 4 only
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho23)*(1-rho12)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho23)*(1-rho12)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 3 and 4
                        indicem =8*i1m+4*i2f+2*i3f+i4m+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3m+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*rho34*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*rho34*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and between loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3f+i4m+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3m+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*rho34*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*rho34*gen[indice]/2;
                                            // recombination happens between loci 1 and 2, between loci 2 and 3 and between loci 3 and 4
                        indicem =8*i1m+4*i2f+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*rho23*rho34*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*rho23*rho34*gen[indice]/2;
                        // cout << indice  << " rec 12, 23 and 34 " << endl;
                      } else {
                    // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho12)*(1-rho23)*(1-rho34)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho12)*(1-rho23)*(1-rho34)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 only
                        indicem =8*i1m+4*i2f+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*(1-rho34)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*(1-rho34)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 only
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*(1-rho34)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*(1-rho34)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 2 and 3
                        indicem =8*i1m+4*i2f+2*i3m+i4m+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3f+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*rho12*(1-rho34)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho12*(1-rho34)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 3 and 4 only
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho23)*(1-rho12)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho23)*(1-rho12)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 3 and 4
                        indicem =8*i1m+4*i2f+2*i3f+i4m+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3m+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*rho34*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*rho34*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and between loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3f+i4m+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3m+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*rho34*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*rho34*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 1 and 2, between loci 2 and 3 and between loci 3 and 4
                        indicem =8*i1m+4*i2f+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*rho23*rho34*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*rho23*rho34*(1-rho45)*gen[indice]/2;
                         // recombination happens between loci 4 and 5 only
                        indicem =8*i1m+4*i2m+2*i3m+i4m+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ rho45*(1-rho23)*(1-rho12)*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho45*(1-rho23)*(1-rho12)*(1-rho34)*gen[indice]/2;
                         // recombination happens between loci 1 and 2 and loci 4 and 5
                        indicem =8*i1m+4*i2f+2*i3f+i4f+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3m+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*(1-rho34)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*(1-rho34)*rho45*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*(1-rho34)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*(1-rho34)*rho45*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 2 and 3 and loci 4 and 5
                        indicem =8*i1m+4*i2f+2*i3m+i4m+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3f+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*rho12*(1-rho34)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho12*(1-rho34)*rho45*gen[indice]/2;
                                            // recombination happens between loci 3 and 4 and loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho23)*(1-rho12)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho23)*(1-rho12)*rho45*gen[indice]/2;
                                            // recombination happens between loci 1 and 2 and between loci 3 and 4 and loci 4 and 5
                        indicem =8*i1m+4*i2f+2*i3f+i4m+16*i5f;
                        indicef =8*i1f+4*i2m+2*i3m+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ rho12*(1-rho23)*rho34*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*(1-rho23)*rho34*rho45*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and between loci 3 and 4 and loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3f+i4m+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho12)*rho34*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho12)*rho34*rho45*gen[indice]/2;
                                            // recombination happens between loci 1 and 2, between loci 2 and 3 and between loci 3 and 4 and loci 4 and 5
                        indicem =8*i1m+4*i2f+2*i3m+i4f+16*i5m;
                        indicef =8*i1f+4*i2m+2*i3f+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho12*rho23*rho34*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho12*rho23*rho34*rho45*gen[indice]/2;
                        // cout << indice  << " rec 12, 23 34 and 45" << endl;
                                    // assume that locus 1 is homozygote and locus 2 is heterozygote
                      }
                    } else if (i2==1&&(i3==1||i4==1||i5==1)){
                    // assume that locus 3 is heterozygote
                      if (i4!=1 && i5!=1){
                    // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho23)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho23)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*gen[indice]/2;
                        // cout << indice  << " rec 23 " << endl;
                    // assume that both locus 3 and 4 can be heterozygote
                      } else if (i5!=1){
                    // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho34)*(1-rho23)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho34)*(1-rho23)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho34)*gen[indice]/2;
                                            // recombination happens between loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho23)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho23)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3f+i4m+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3m+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*rho34*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho34*gen[indice]/2;
                        // cout << indice  << " rec 23 and 34 " << endl;
                    //
                      } else {
                     // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho34)*(1-rho23)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho34)*(1-rho23)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho34)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho34)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho23)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho23)*(1-rho45)*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3f+i4m+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3m+i4f+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*rho34*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho34*(1-rho45)*gen[indice]/2;
                         // recombination happens between loci 2 and 3 and loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3f+i4f+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3m+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho23*(1-rho34)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*(1-rho34)*rho45*gen[indice]/2;
                         // recombination happens between loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3m+i4m+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ rho45*(1-rho23)*(1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho45*(1-rho23)*(1-rho34)*gen[indice]/2;
                                            // recombination happens between loci 3 and 4 and loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho23)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho23)*rho45*gen[indice]/2;
                                            // recombination happens between loci 2 and 3 and loci 3 and 4 and loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3f+i4m+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3m+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ rho23*rho34*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho23*rho34*rho45*gen[indice]/2;
                        // cout << indice  << " rec 23 34 and 45" << endl;
                      }
                  // loci 1 and 2 are homozygote. Therefore loci 3 and 4  have to be heterozygote.
                    } else if(i3==1&&(i4==1||i5==1)){
                      if (i5!=1){
                      // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho34)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho34)*gen[indice]/2;
                                        // recombination happens between loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho34*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*gen[indice]/2;
                        // cout << indice  << " rec  34  " << endl;
                      } else {
                      // add the frequency of non-recombining genotypes.
                        hap[indicem]=hap[indicem]+ (1-rho34)*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho34)*(1-rho45)*gen[indice]/2;
                                        // recombination happens between loci 3 and 4
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5m;
                        hap[indicem]=hap[indicem]+ rho34*(1-rho45)*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*(1-rho45)*gen[indice]/2;
                        // recombination happens between loci 4 and 5
                        indicem =8*i1m+4*i2m+2*i3m+i4m+16*i5f;
                        indicef =8*i1f+4*i2f+2*i3f+i4f+16*i5m;
                        hap[indicem]=hap[indicem]+ (1-rho34)*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ (1-rho34)*rho45*gen[indice]/2;
                        // recombination happens between loci 3 and 4 and 4 and 5
                        indicem =8*i1m+4*i2m+2*i3m+i4f+16*i5m;
                        indicef =8*i1f+4*i2f+2*i3f+i4m+16*i5f;
                        hap[indicem]=hap[indicem]+ rho34*rho45*gen[indice]/2;
                        hap[indicef]=hap[indicef]+ rho34*rho45*gen[indice]/2;
                        // cout << indice  << " rec  34 and 45 " << endl;
                      } 
                    } else if(i4==1&&i5==1){
                    // add the frequency of non-recombining genotypes.
                      hap[indicem]=hap[indicem]+ (1-rho45)*gen[indice]/2;
                      hap[indicef]=hap[indicef]+ (1-rho45)*gen[indice]/2;
                      // recombination happens between loci 4 and 5
                      indicem =8*i1m+4*i2m+2*i3m+i4m+16*i5f;
                      indicef =8*i1f+4*i2f+2*i3f+i4f+16*i5m;
                      hap[indicem]=hap[indicem]+ rho45*gen[indice]/2;
                      hap[indicef]=hap[indicef]+ rho45*gen[indice]/2;
                      // cout << indice  << " rec  45 " << endl;    
                    } else {
                                        // // add the frequency of non-recombining genotypes.
                      hap[indicem]=hap[indicem]+ gen[indice]/2;
                      hap[indicef]=hap[indicef]+ gen[indice]/2;
                      // cout << indice  << "no rec" << endl;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
}
}



void test_fix(const int& n_gen, const double & locus, int & T, bool & a, const string& name){
    // for a given locus, t record the time to fixation of one of the two alleles 0 or 1, and a save which allele fixed.
  if (T<0){
    if (locus==0){
      T=n_gen;
//            cout << "fixation "<< name << " allele 0 gen. "<< n_gen << endl;
    } else if(locus==1){
      T=n_gen;
      a=1;
//            cout << "fixation "<< name << " allele 1 gen. "<< n_gen <<endl;
    }
  }
}

void track_alleles(const array<double, 32>& hap, const int& n_gen, int& T1, int& T2, int& T3, int& T4, int& T5, bool& a1, bool& a2, bool& a3, bool& a4, bool& a5){
  double locus1=0, locus2=0, locus3=0, locus4=0, locus5=0;
  int indicem;
    // calculate the frequency of allele 1 at each locus
  for (int i1m=0; i1m<=1; i1m++){
    for (int i2m=0; i2m<=1; i2m++){
      for (int i3m=0; i3m<=1; i3m++ ){
        for (int i4m=0; i4m<=1; i4m++){
         for (int i5m=0; i5m<=1; i5m++){
          indicem =8*i1m+4*i2m+2*i3m+i4m+16*i5m;
          locus1=locus1+i1m*hap[indicem];
          locus2=locus2+i2m*hap[indicem];
          locus3=locus3+i3m*hap[indicem];
          locus4=locus4+i4m*hap[indicem];
          locus5=locus5+i5m*hap[indicem];
        }
      }
    }
  }
}
    // cout << n_gen << " " << locus1 << " " << locus2 << " " << locus3 << " " << locus4<< " "<< locus5<<endl;
    // check for fixation of said allele, and when it happened.
test_fix(n_gen, locus1, T1, a1, "locus 1");
test_fix(n_gen, locus2, T2, a2, "locus 2");
test_fix(n_gen, locus3, T3, a3, "locus 3");
test_fix(n_gen, locus4, T4, a4, "locus 4");
test_fix(n_gen, locus5, T5, a5, "locus 5");
}

void add_drift(gsl_rng* r, array<double, 32>& hap, const int& Nind){
  unsigned int haplotype_ind_count[32];
  double hap_freq[32];
    // generate drift through multinomial sampling of gametes frequencies
  for (int i=0;i<32;i++){
    hap_freq[i]=hap[i];
//        cout << hap_freq[i];
  }
//    cout <<endl;

  //  cout << "before drift"<< endl;
  gsl_ran_multinomial(r, 32, Nind, hap_freq, haplotype_ind_count);

//    cout << "after drift"<< endl;
    // covert back raw numbers to frequencies
  for (int i=0;i<32;i++){
    hap[i]=(double)haplotype_ind_count[i]/(double)Nind;
  }
}
// run is the main function. There is three version of it. The definition has been overloaded, i.e. each version has different number of parameters.
// One version is the main function used to indeed run the simulations. The second one print in an extra the frequency at each generation, while the third one also output the fitness of each haplotype.
void run(gsl_rng* r, int& T_locus1, int& T_locus2, int& T_locus3, int& T_locus4, int& T_locus5, bool& a1, bool& a2, bool& a3, bool& a4, bool& a5, const int& n_gen, const int& n_ind, const double& r12, const double& r23, const double& r34, double& r45, const array<double, 1024>& fitness_table, const array<double, 32>& initial_freq){

  int indicem, indicef, indice;
  double wbar;


  array<double, 32> haplotype_freq=initial_freq; // define an array to store the frequency of all 16 different haplotypes.
    array<double, 1024> genotype_freq; // define an array to store the frequency of all 256 different genotypes.


    // calculate the genotype frequency based on the haplotype assuming no assortative mating.
    for (int gen=0; gen<n_gen; gen++){
      //cout <<gen <<endl;
      for (int i1m=0; i1m<=1; i1m++){
        for (int i2m=0; i2m<=1; i2m++){
          for (int i3m=0; i3m<=1; i3m++ ){
            for (int i4m=0; i4m<=1; i4m++){
              for (int i5m=0; i5m<=1; i5m++){
               indicem =16*i5m+8*i1m+4*i2m+2*i3m+i4m;

               for (int i1f=0; i1f<=1; i1f++){
                for (int i2f=0; i2f<=1; i2f++){
                 for (int i3f=0; i3f<=1; i3f++ ){
                  for (int i4f=0; i4f<=1; i4f++){
                   for (int i5f=0; i5f<=1; i5f++){
                    indicef =16*i5f+8*i1f+4*i2f+2*i3f+i4f;
                    indice = 32*indicem+indicef;
                    genotype_freq[indice]=haplotype_freq[indicem]*haplotype_freq[indicef];
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
        // apply selection to young adults; this happens in two stages. First raw selection coefficient are multiplied to the frequencies of the genotypes.
        //In a second step, we correct the selection value by the mean fitness wbar of the population
wbar=0;
for (int i=0; i<1024; i++){
 //           cout << genotype_freq[i] <<endl;
  genotype_freq[i]=genotype_freq[i]*fitness_table[i];
  wbar=wbar+genotype_freq[i];
}

    //        cout << "genotypes"<<endl;

    // correction by the mean fitness
for (int i=0; i<1024; i++){
  genotype_freq[i]=genotype_freq[i]/wbar;
//            cout << i <<" "<<genotype_freq[i] <<endl;
}

//    cout << "sel done" << endl;

        // empty the gamete vector.
for (int i=0; i<32; i++){
  haplotype_freq[i]=0;
}
        // calculate the gamete frequency
gametes_prod(haplotype_freq,genotype_freq,r12,r23,r34,r45);

   //     cout << "gametes produces"<<endl;
        // add the drift step
add_drift(r, haplotype_freq, n_ind);

     //   cout << "drift"<<endl;
      //  fprintf(fichierO, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", haplotype_freq[0],haplotype_freq[1],haplotype_freq[2],haplotype_freq[3],haplotype_freq[4],haplotype_freq[5],haplotype_freq[6],haplotype_freq[7],haplotype_freq[8],haplotype_freq[9],haplotype_freq[10],haplotype_freq[11],haplotype_freq[12],haplotype_freq[13],haplotype_freq[14],haplotype_freq[15]);

        // check whether any locus got fixed for a specific allele
track_alleles(haplotype_freq, gen, T_locus1, T_locus2, T_locus3, T_locus4, T_locus5, a1, a2, a3, a4, a5);

        // cout << T_locus1<< "," << T_locus2<<"," <<T_locus3<<","<< T_locus4<<","<< T_locus5<< endl;
        // if all loci are no longer polymorphic stop the iteration process
if(T_locus1>=0&& T_locus2>=0&& T_locus3>=0&& T_locus4>=0&T_locus5>=0){
      //      cout<<"done"<<endl;
  break;
}
}
}


int main(int argc, char *argv[])
{
    // check that the call of the program has the right number of parameters: 4, 5 or 6
  if (argc!=5 && argc!=6 && argc!=7)
  {
    cout << "need four mandatory parameters\n the parameter file name, the output file, number of iterations, and the seed\n the parameter file contains the different parameters in this order s1, s2, s3, s4, e12, e13, e14, e23, e24, e34, r12,r23,r34, h12, h13, h14, h23, h24, h34 \n optional parameter are the name of the trajectory file as well as a fitness file to save the fitness matrix\n";
    return -1;
  }

    // declare variables:
    // selection: s1,s2,s3,s4 ; epistasis e12, e13, e14 e23 e24 e34; dominance of the epistasis h12, h13, h14, h23, h24, h34; recombination r12, r23, r34
    // initial frequency f1,...f16.

  //printf ("seed = %lu\n", gsl_rng_default_seed);


  double s1, s2, s3, s4, e12, e13, e14, e23, e24, e34, r12,r23,r34,r45, value, epis_12, epis_13, epis_14, epis_23, epis_24, epis_34;
  int h12, h13, h14, h23, h24, h34, indice, indicem, indicef, i1, i2,i3,i4, n_gen, n_ind;

    array<double, 1024> fitness_table; // create fitness array
  array<double, 32> initial_freq; // initial frequencies array

  int T_locus1=-1, T_locus2=-1, T_locus3=-1, T_locus4=-1, T_locus5=-1;
  bool allele1=0, allele2=0, allele3=0,allele4=0, allele5=0;

    // read the aprameter file
  read_parameter(argv[1],s1, s2, s3, s4, e12, e13, e14, e23, e24, e34, r12,r23,r34,r45,h12, h13, h14, h23, h24, h34,n_gen, n_ind, initial_freq);

    // number of ierations of the sme aprameter set
  int nb_int = atoi(argv[3]);
  // choose the seed; if -1 it is random
  int seed = atoi(argv[4]);
  if (seed==-1){
    srand (time(NULL));
    seed=rand();
  }

  // initialize gsl random generator number for the multinomial sampling
  const gsl_rng_type *T;
  gsl_rng *r;
  gsl_rng_env_setup();

  T=gsl_rng_default;
  r=gsl_rng_alloc(T);
  gsl_rng_set(r, seed);

    // open output file
  FILE * fichierO;
  fichierO= fopen(argv[2],"a");
  if (fichierO == NULL) {
    cout << "wrong output file" << endl;
    return -1;
  }

    // rint header of the output file. It contains all the parameters value of the run, to ensure reapatability.
  fprintf(fichierO, "Parameters: seed=%i, s1=%f, s2=%f, s3=%f, s4=%f, e12=%f, e13=%f, e14=%f, e23=%f, e24=%f, e34=%f, r12=%f, r23=%f, r34=%f, r45=%f, h12=%i, h13=%i, h14=%i, h23=%i, h24=%i, h34=%i , n_gen=%i, n_ind=%i ", seed, s1, s2, s3, s4, e12, e13, e14, e23, e24, e34, r12,r23,r34,r45, h12, h13, h14, h23, h24, h34, n_gen, n_ind  );
  fprintf(fichierO, "initial freq: %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n", initial_freq[0],initial_freq[1],initial_freq[2],initial_freq[3],initial_freq[4],initial_freq[5],initial_freq[6],initial_freq[7],initial_freq[8],initial_freq[9],initial_freq[10],initial_freq[11],initial_freq[12],initial_freq[13],initial_freq[14],initial_freq[15],initial_freq[16],initial_freq[17],initial_freq[18],initial_freq[19],initial_freq[20],initial_freq[21],initial_freq[22],initial_freq[23],initial_freq[24],initial_freq[25],initial_freq[26],initial_freq[27],initial_freq[28],initial_freq[29],initial_freq[30],initial_freq[31]);

  // check that initial frequencies do sum up to one
  value=0;
  for (int i=0; i<32; i++){
    value=value+initial_freq[i];
  }
  if (fabs(value-1.0)>1e-9){
    fprintf(fichierO, "wrong haplotype frequencies summing to: %f ", value);
    fprintf(fichierO, "difference is: %e", fabs(value-1.0));
    fclose(fichierO);
    return -1;
  }


//    cout << "input read" << endl;
//    cout << "simulating 4 loci with the following parameter"<<endl;
//    cout << "selection: s1="<<s1 <<", s2"<<s2 <<", s3="<<s3 <<", s4="<<s4<<endl;
//    cout << "epistasis: e12="<<e12 <<", e13="<< e13 <<", e14="<<e14 <<", e23="<<e23 <<", e24="<<e24 <<", e34="<< e34 <<endl;
//    cout << "recombination: r12="<<r12 <<", r23="<<r23<<", r34="<<r34<<endl;
//    cout << "dominance: h12="<<h12 <<", h13="<< h13 <<", h14="<<h14 <<", h23="<<h23 <<", h24="<<h24 <<", h34="<< h34 <<endl;
//    cout << "number gen.= "<< n_gen << ", n_ind= " << n_ind <<", seed="<<seed<< endl;
//    cout << "initial frequencies:"<<endl;
//    for (int i1m=0; i1m<=1; i1m++){
//        for (int i2m=0; i2m<=1; i2m++){
//            for (int i3m=0; i3m<=1; i3m++ ){
//                for (int i4m=0; i4m<=1; i4m++){
//                    indicem =8*i1m+4*i2m+2*i3m+i4m;
//                    cout << "hap"<<i1m <<i2m <<i3m<<i4m<< ": f=" <<initial_freq[indicem]<<endl;
//                }
//            }
//        }
//    }

    // write the fitness table
  for (int i1m=0; i1m<=1; i1m++){
    for (int i2m=0; i2m<=1; i2m++){
      for (int i3m=0; i3m<=1; i3m++ ){
        for (int i4m=0; i4m<=1; i4m++){
         for (int i5m=0; i5m<=1; i5m++){
          indicem =8*i1m+4*i2m+2*i3m+i4m+16*i5m;
          for (int i1f=0; i1f<=1; i1f++){
           for (int i2f=0; i2f<=1; i2f++){
            for (int i3f=0; i3f<=1; i3f++ ){
             for (int i4f=0; i4f<=1; i4f++){
              for (int i5f=0; i5f<=1; i5f++){
               indicef =8*i1f+4*i2f+2*i3f+i4f+16*i5f;
               indice  = 32*indicem+indicef;
               i1=i1m+i1f;i2=i2m+i2f;i3=i3m+i3f;i4=i4m+i4f;
               if(i1==1&&i2==1){epis_12=1+h12*e12 *i1*i2;}else{epis_12=pow(1+e12,i1*i2);}
               if(i1==1&&i3==1){epis_13=1+h13*e13 *i1*i3;}else{epis_13=pow(1+e13,i1*i3);}
               if(i1==1&&i4==1){epis_14=1+h14*e14 *i1*i4;}else{epis_14=pow(1+e14,i1*i4);}
               if(i2==1&&i3==1){epis_23=1+h23*e23 *i2*i3;}else{epis_23=pow(1+e23,i2*i3);}
               if(i2==1&&i4==1){epis_24=1+h24*e24 *i2*i4;}else{epis_24=pow(1+e24,i2*i4);}
               if(i3==1&&i4==1){epis_34=1+h34*e34 *i3*i4;}else{epis_34=pow(1+e34,i3*i4);}
               fitness_table[indice]=pow(1+s1,i1)*pow(1+s2,i2)*pow(1+s3,i3)*pow(1+s4,i4)*epis_12*epis_13*epis_14*epis_23*epis_24*epis_34;
             }
           }
         }
       }
     }
   }
 }
}
}
}


//    for (int i=0; i<256;i++){
//    cout << fitness_table[i] <<endl;
//  }
//  cout << "start sim"<<endl;


for (int j=0; j<nb_int;j++){
//    cout << "iteration "<< j<< "/"<<nb_int<< endl;
    T_locus1=-1; T_locus2=-1; T_locus3=-1; T_locus4=-1; T_locus5=-1; // initialize fixation time
    allele1=0; allele2=0; allele3=0; allele4=0; allele5=0; // initialize allele fixation place holder
    // call main function
    run(r, T_locus1, T_locus2, T_locus3, T_locus4, T_locus5, allele1, allele2, allele3, allele4, allele5, n_gen, n_ind, r12, r23, r34, r45, fitness_table, initial_freq);
    fprintf(fichierO,"%i %i %i %i %i %i %i %i %i %i\n", allele1, T_locus1, allele2, T_locus2, allele3, T_locus3, allele4, T_locus4, allele5, T_locus5);
  }



  fclose(fichierO);


  return 1;
}
