---
title: "R Notebook: The Intricate Dynamics of Hybrid Speciation"
output:
  html_notebook:
    code_folding: hide
    highlight: textmate
    theme: cosmo
    toc: yes
    toc_float: yes
---




```{r} 
# add name of folder containing simulations for the codominant model
list_folder_4L_cod=c("output_sym_dom_rfunction_pi_0d5")
# add name of folder containing simulations for the recessive model
list_folder_4L_rec=c()
```

```{r}
source("functions.R")
```

## Example for the ABAB architecture

### Recombination rate vs. probability of hybrid speciation codominant case

```{r probability of hybrid speciation codominant case, message=TRUE, warning=FALSE}
temp=master_data_4L_cod[sel()&master_data_4L_cod$N==10000&adj_ABAB()&equidistant()&master_data_4L_cod$f0101==0.5&master_data_4L_cod$f1010==0.5,]
x=(temp$p_h1001+temp$p_h0110)/temp$n_rep
plot(temp$r12,x,pch=16,ylim=c(0,1),col="red",xlim=c(0.0001,.5),main="",xlab = "Recombination",ylab="Hyb. spec. prob.",log="x",mgp=c(2,1,0))
arrows(temp$r12,x-sqrt(x*(1-x)/(temp$n_rep-1)),temp$r12,x+sqrt(x*(1-x)/(temp$n_rep-1)),angle=90,code=3,length=.02,col="red")
```

### Recombination rate vs. Time to speciation

```{r time speciation adj ABAB, warning=FALSE}

temp=master_data_4L_cod[sel()&adj_ABAB()&equidistant()&master_data_4L_cod$f1010==0.5&master_data_4L_cod$f0101==0.5,]
make_plot_time(temp$r12,temp$N,temp$p_h1001,temp$p_h0110,temp$t_h1001,temp$t_h0110,temp$t_sq_h1001,temp$t_sq_h0110)
```

### Recombination rate vs. probability of hybrid speciation recessive case


```{r probability of hybrid speciation recessive case}
temp=master_data_4L_rec[sel(data = master_data_4L_rec)&master_data_4L_rec$N==10000&adj_ABAB(data=master_data_4L_rec)&equidistant(data=master_data_4L_rec)&master_data_4L_rec$f0101==0.5&master_data_4L_rec$f1010==0.5,]
x=(temp$p_h1001+temp$p_h0110)/temp$n_rep
plot(temp$r12,x,pch=16,ylim=c(0,1),col="red",xlim=c(10^-4,0.5),main="",xlab = "Recombination",ylab="Hyb. spec. prob.",log="x",mgp=c(2,1,0))
arrows(temp$r12,x-sqrt(x*(1-x)/(temp$n_rep-1)),temp$r12,x+sqrt(x*(1-x)/(temp$n_rep-1)),angle=90,code=3,length=.02,col="red")
```

